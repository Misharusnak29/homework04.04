/* Теоретичні питання
1. let,const,var

2. const str = String("Це рядок");

3. console.log(typeof змінна)

4. тому що тип змінної string */

//    Практичні завдання
// 1.

const number = 11 ;

console.log(typeof number);

// 2.

const firstName = "Михайло";

const lastName = "Руснак";

console.log(`Мене звати ${firstName} ${lastName}`);

// 3.

const number2 = 10;

console.log(`Hello world ${number2}`)